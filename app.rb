require 'mini_magick'

logo_sizes = [
  { 'ldpi' => '36x36' },
  { 'mdpi' => '48x48' },
  { 'hdpi' => '72x72' },
  { 'xhdpi' => '96x96' },
  { 'xxhdpi' => '144x144' },
  { 'xxxhdpi' => '192x192' }
]

logo_sizes.each do |item|
  name = item.keys[0]
  size = item[name]
  logo = MiniMagick::Image.open 'logo.png'
  logo.resize size
  logo.write "output/drawable-#{name}-icon.png"
  puts "wrote output/drawable-#{name}-icon.png"
end

splash_sizes = [
  { 'ldpi' => { portrait: '200x320', landscape: '320x200' } },
  { 'mdpi' => { portrait: '320x480', landscape: '380x320' } },
  { 'hdpi' => { portrait: '480x800', landscape: '800x480' } },
  { 'xhdpi' => { portrait: '720x1280', landscape: '1280x720' } },
  { 'xxhdpi' => { portrait: '960x1600', landscape: '1600x960' } },
  { 'xxxhdpi' => { portrait: '1280x1920', landscape: '1920x1280' } }
]

splash_sizes.each do |item|
  name = item.keys[0]
  size = item[name]
  logo = MiniMagick::Image.open 'splash.png'
  logo.resize size[:portrait]
  logo.write "output/drawable-#{name}-screen.png"
  puts "wrote output/drawable-#{name}-screen.png"

  logo = MiniMagick::Image.open 'splash-landscape.png'
  logo.resize size[:landscape]
  logo.write "output/drawable-land-#{name}-screen.png"
  puts "wrote output/drawable-land-#{name}-screen.png"
end
