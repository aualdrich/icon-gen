Generates icons and splash screens for mobile. To run:

1. Drop your source files called logo.png, splash.png, and splash-landscape.png into the root directory.
2. bundle install
3. ruby app.rb
4. Files are written to the output directory.